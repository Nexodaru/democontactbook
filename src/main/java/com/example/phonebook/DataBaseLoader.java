package com.example.phonebook;

import com.example.phonebook.models.Contact;
import com.example.phonebook.models.User;
import com.example.phonebook.persistence.repositories.ContactRepository;
import com.example.phonebook.persistence.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DataBaseLoader implements CommandLineRunner {

    private final ContactRepository contactRepository;
    private final UserRepository userRepository;

    @Autowired
    public DataBaseLoader(ContactRepository contactRepository, UserRepository userRepository) {
        this.contactRepository = contactRepository;
        this.userRepository = userRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        User owner = new User("Test1234567", "abc@abc.de", "test");
        this.userRepository.save(owner);
        this.contactRepository.save(new Contact(owner, "Franz", "Schwarzer", List.of(), ""));
    }
}
