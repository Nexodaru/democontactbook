package com.example.phonebook.dtos;

import com.example.phonebook.models.Contact;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ShortContactItem {
    String id;
    private String forename;
    private String surname;

    public ShortContactItem(String id, String forename, String surname) {
        this.id = id;
        this.forename = forename;
        this.surname = surname;
    }

    public static ShortContactItem fromContact(Contact contact) {
        return new ShortContactItem(contact.getId(), contact.getForename(), contact.getSurname());
    }
}
