package com.example.phonebook.dtos;

import com.example.phonebook.models.Contact;
import com.example.phonebook.models.TelephoneNumber;
import com.example.phonebook.models.User;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ContactItem {
    private String id;
    private String forename;
    private String surname;
    private List<TelephoneNumber> numbers;
    private String email;

    public ContactItem(String id, String forename, String surname, List<TelephoneNumber> numbers, String email) {
        this.id = id;
        this.forename = forename;
        this.surname = surname;
        this.numbers = numbers;
        this.email = email;
    }

    public static ContactItem fromContact(Contact contact) {
        return new ContactItem(contact.getId(), contact.getForename(), contact.getSurname(), contact.getNumbers(), contact.getEmail());
    }

    public Contact toContact(User owner) {
        return new Contact(owner, forename, surname, numbers, email);
    }
}
