package com.example.phonebook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PhoneBookApplication {
//    @Value("${MONGO_HOST:localhost}")
//    private String host;
//    @Value("${MONGO_PORT:27017}")
//    private int port;
//    @Value("${MONGO_USER:root}")
//    private String username;
//    @Value("${MONGO_PASSWORD:example}")
//    private String password;
//    @Value("${MONGO_AUTH_DB:admin}")
//    private String authDatabase;
//    @Value("${MONGO_DB:test}")
//    private String database;
//
//    @Bean
//    public MongoClientFactoryBean mongo() {
//        MongoClientFactoryBean mongo = new MongoClientFactoryBean();
//        mongo.setHost(host);
//        mongo.setPort(port);
//        mongo.setCredential(new MongoCredential[]{MongoCredential.createCredential(username, authDatabase, password.toCharArray())});
//
//        return mongo;
//    }

    public static void main(String[] args) {
        SpringApplication.run(PhoneBookApplication.class, args);
    }


}

