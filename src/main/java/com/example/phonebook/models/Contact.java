package com.example.phonebook.models;


import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.util.List;

@Getter
@Setter
public class Contact {
    private String id;
    @DBRef(lazy = true)
    @Indexed
    private User owner;
    private String forename;
    private String surname;
    private List<TelephoneNumber> numbers;
    private String email;

    public Contact() {
    }

    public Contact(User owner, String forename, String surname, List<TelephoneNumber> numbers, String email) {
        this.owner = owner;
        this.forename = forename;
        this.surname = surname;
        this.numbers = numbers;
        this.email = email;
    }
}

