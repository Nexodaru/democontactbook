package com.example.phonebook.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TelephoneNumber {
    private TelephoneNumberType type;
    private String value;

    public TelephoneNumber(TelephoneNumberType type, String value) {
        this.type = type;
        this.value = value;
    }
}
