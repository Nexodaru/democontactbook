package com.example.phonebook.models;

public enum TelephoneNumberType {
    BUSINESS, MOBIL, PRIVATE, MAIN, FAX_PRIVATE, FAX_BUSINESS, PAGER, OTHER
}
