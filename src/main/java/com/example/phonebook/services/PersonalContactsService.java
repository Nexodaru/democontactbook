package com.example.phonebook.services;

import com.example.phonebook.dtos.ContactItem;
import com.example.phonebook.dtos.ShortContactItem;
import com.example.phonebook.models.Contact;
import com.example.phonebook.models.User;
import com.example.phonebook.persistence.repositories.ContactRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.RequestScope;

import java.security.AccessControlException;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
@RequestScope
public class PersonalContactsService {

    private final String username;

    @Autowired
    private ContactRepository contactRepository;

    private static final Logger logger = LoggerFactory.getLogger(PersonalContactsService.class);

    public PersonalContactsService() {
        this.username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    public List<ShortContactItem> getContactsAsCompactItem() {
        List<Contact> contacts = contactRepository.findByOwnerIdAndOnlyIncludeIDAndName(username);
        logger.info(String.valueOf(contacts));
        return contacts.stream()
                .sorted(Comparator.comparing(Contact::getForename))
                .map(ShortContactItem::fromContact).collect(Collectors.toList());
    }

    public ContactItem updateContact(ContactItem contactItem) {
        Optional<Contact> contact = contactRepository.findById(contactItem.getId());
        if (contact.isPresent()) {
            contactRepository.save(contactItem.toContact(User.builder().username(username).build()));
            return ContactItem.fromContact(contact.get());
        }
        throw new AccessControlException("User is not owner of this contact");
    }

    public ContactItem createContact(ContactItem contactItem) {
        return ContactItem.fromContact(contactRepository.save(contactItem.toContact(User.builder().username(username).build())));
    }

    public void removeContact(String contactId) {
        contactRepository.removeByIdAndOwnerUsername(contactId, username);
    }

    public Optional<ContactItem> getContact(String contactId) {
        Optional<Contact> contact = contactRepository.findByIdAndOwnerUsername(contactId, username);
        return contact.map(ContactItem::fromContact);
    }

}
