package com.example.phonebook.security;

public class SecurityConstants {
    public static final String AUTH_BASE_PATH = "/api/auth";
    public static final String SIGN_UP_URL = AUTH_BASE_PATH + "/signup";
    public static final String SIGN_IN_URL = AUTH_BASE_PATH + "/signin";
    public static final String HEADER_NAME = "Authorization";
}
