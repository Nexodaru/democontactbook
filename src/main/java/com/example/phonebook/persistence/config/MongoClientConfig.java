//package com.example.phonebook.persistence.config;
//
//import com.mongodb.MongoClientSettings;
//import com.mongodb.MongoCredential;
//import com.mongodb.ServerAddress;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;
//import org.springframework.data.mongodb.core.MongoTemplate;
//import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
//
//import static java.util.Collections.singletonList;
//
//@Configuration
//@EnableMongoRepositories(basePackages = "com.example.phonebook.persistence.repositories")
//public class MongoClientConfig extends AbstractMongoClientConfiguration {
//    @Value("${spring.data.mongodb.host}")
//    private String host;
//    @Value("${spring.data.mongodb.port}")
//    private int port;
//    @Value("${spring.data.mongodb.username}")
//    private String username;
//    @Value("${spring.data.mongodb.password}")
//    private String password;
//    @Value("${spring.data.mongodb.authentication-database}")
//    private String authDatabase;
//    @Value("${spring.data.mongodb.database}")
//    private String database;
//
//    @Override
//    protected void configureClientSettings(MongoClientSettings.Builder builder) {
//        System.out.println(host + port + username + password + authDatabase + database);
//
//        builder
//                .credential(MongoCredential.createCredential(username, authDatabase, password.toCharArray()))
//                .applyToClusterSettings(settings -> {
//                    settings.hosts(singletonList(new ServerAddress(host, port)));
//                });
//    }
//
//    @Bean
//    public String values() {
//        return host + port + username + password + authDatabase + database;
//    }
//
//    @Override
//    protected String getDatabaseName() {
//        return database;
//    }
//
//    @Bean
//    public MongoTemplate mongoTemplate() throws Exception {
//
//        return new MongoTemplate(mongoClient(), getDatabaseName());
//    }
//}
