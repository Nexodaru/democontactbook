package com.example.phonebook.persistence.repositories;

import com.example.phonebook.models.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;


public interface UserRepository extends MongoRepository<User, String> {
    Optional<User> findByUsername(String username);

    Boolean existsByUsernameOrEmail(String username, String email);
}
