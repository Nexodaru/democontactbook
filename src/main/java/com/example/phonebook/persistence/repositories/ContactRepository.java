package com.example.phonebook.persistence.repositories;

import com.example.phonebook.models.Contact;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ContactRepository extends MongoRepository<Contact, String> {

    //List<ShortContact> findAllShortContactByOwnerId(String ownerId);
    Optional<Contact> findByIdAndOwnerUsername(String id, String ownerId);

    void removeByIdAndOwnerUsername(String contactId, String ownerId);

    @Query(value = "{'owner' : ?0}", fields = "{surname : 1, forename : 1, _id : 1}")
    List<Contact> findByOwnerIdAndOnlyIncludeIDAndName(String ownerId);

}
