package com.example.phonebook.controller;

import com.example.phonebook.models.Contact;
import com.example.phonebook.models.User;
import com.example.phonebook.persistence.repositories.ContactRepository;
import com.example.phonebook.persistence.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/test")
public class TestController {

    @Autowired
    ContactRepository contactRepository;

    @GetMapping("")
    public List<Contact> get() {
        return contactRepository.findAll();
    }
}
