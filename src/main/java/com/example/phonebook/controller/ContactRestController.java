package com.example.phonebook.controller;

import com.example.phonebook.dtos.ContactItem;
import com.example.phonebook.dtos.ShortContactItem;
import com.example.phonebook.security.jwt.AuthTokenFilter;
import com.example.phonebook.services.PersonalContactsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/contacts")
public class ContactRestController {
    @Autowired
    private ObjectProvider<PersonalContactsService> personalContactsServiceProvider;
    private static final Logger logger = LoggerFactory.getLogger(AuthTokenFilter.class);

    @GetMapping("")
    public List<ShortContactItem> getContacts() {
        return personalContactsServiceProvider.getObject().getContactsAsCompactItem();
    }

    @PutMapping("")
    public ContactItem updateContact(@Valid @RequestBody ContactItem contactItem) {
        return personalContactsServiceProvider.getObject().updateContact(contactItem);

    }

    @PostMapping("")
    public ResponseEntity<ContactItem> createContact(@Valid @RequestBody ContactItem contactItem) {
        ContactItem createdContact = personalContactsServiceProvider.getObject().createContact(contactItem);
        return new ResponseEntity<>((createdContact), HttpStatus.CREATED);
    }

    @DeleteMapping("/{contactId}")
    public void createContact(@PathVariable("contactId") String contactId) {
        personalContactsServiceProvider.getObject().removeContact(contactId);
    }

    @GetMapping("/{contactId}")
    public ContactItem getContact(@PathVariable("contactId") String contactId) {
        Optional<ContactItem> contactItem = personalContactsServiceProvider.getObject().getContact(contactId);
        if (contactItem.isPresent()) {
            return contactItem.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "You didnt own such a contact!");
        }
    }
}
