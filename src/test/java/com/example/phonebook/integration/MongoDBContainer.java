package com.example.phonebook.integration;

import org.testcontainers.containers.GenericContainer;

public class MongoDBContainer extends GenericContainer<MongoDBContainer> {

    private static final int PORT = 27017;

    public MongoDBContainer(final String image) {
        super(image);
        this.addExposedPort(PORT);
    }

    public int getMappedPort() {
        return this.getMappedPort(PORT);
    }


//    @Override
//    public void stop() {
//        // let the JVM handle the shutdown
//    }
}
