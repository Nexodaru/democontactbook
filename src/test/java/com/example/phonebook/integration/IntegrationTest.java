package com.example.phonebook.integration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.wait.strategy.Wait;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.Map;
import java.util.function.Supplier;

@SpringBootTest
@Testcontainers
public class IntegrationTest {
    private static final String testUsername = "test123";
    private static final String testPassword = "testpw";

    @Container
    private static final MongoDBContainer MONGO_DB = new MongoDBContainer("mongo:4.2.5").withEnv(Map.of(
            "MONGO_INITDB_ROOT_USERNAME", "root",
            "MONGO_INITDB_ROOT_PASSWORD", "example"
    ));

    @DynamicPropertySource
    private static void mongoDBProperties(final DynamicPropertyRegistry registry) {
        registry.add("spring.data.mongodb.host}", MONGO_DB::getContainerIpAddress);
        registry.add("spring.data.mongodb.port}", MONGO_DB::getMappedPort);
        registry.add("spring.data.mongodb.username}", () -> testUsername);
        registry.add("spring.data.mongodb.password}", () -> testPassword);
        registry.add("spring.data.mongodb.authentication-database", () -> "root");
        registry.add("spring.data.mongodb.database", () -> "root");
    }

}
