package com.example.phonebook.integration;

import com.example.phonebook.controller.AuthController;
import com.example.phonebook.dtos.RegisterRequest;
import com.example.phonebook.models.User;
import com.example.phonebook.security.SecurityConstants;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.AutoConfigureDataMongo;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class TestingAuthController extends IntegrationTest {

    @LocalServerPort
    private int port;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    AuthController authController;

    @Autowired
    private MongoTemplate mongo;

    public final User testUser = new User("test123", "test@test.de", "test");

    @Test
    public void contextLoads() throws Exception {
        assertThat(authController).isNotNull();
    }


    @Test
    void testConnection() {
        final String uuid = UUID.randomUUID().toString();
        this.mongo.createCollection(uuid);
        assertThat(this.mongo.collectionExists(uuid)).isTrue();
    }

    @Before
    public void setup() {

    }

    @Test
    public void signup() throws Exception {
        mockMvc.perform(post(SecurityConstants.SIGN_UP_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testUser))
        ).andExpect(status().isOk()).andDo(print());
    }


}
